require('module-alias/register');
const { ApolloServer, makeExecutableSchema } = require('apollo-server');
const resolvers = require('@resolvers/index');
const connectDB = require('@config/db');
const { getUser } = require('@helpers/helpers');
const { typeDefs } = require('@schemas/index');

connectDB();

const schema = makeExecutableSchema({
  typeDefs: [...typeDefs],
  resolvers
});

const server = new ApolloServer({
  schema,
  context: async ({ req }) => ({
    user: await getUser(req)
  })
});

server.listen().then(({ url }) => {
  console.log(`Server running on ${url}`);
});