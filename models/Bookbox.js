const mongoose = require('mongoose');

const BookBoxSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  description: {
    type: String,
    required: true,
    trim: true
  },
  portrait: {
    type: String,
    required: true,
    trim: true,
    default: 'https://picsum.photos/200'
  },
  language: {
    type: String,
    required: true,
    trim: true
  },
  genre: {
    type: Array,
    required: true,
    trim: true
  },
  author: [{
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  }],
  created: {
    type: String,
    required: true
  }
});

const BookBox = mongoose.model('BookBox', BookBoxSchema);

module.exports = BookBox;