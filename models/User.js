const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
  nickname: {
    type: String,
    required: true,
    trim: true,
    unique: true
  },
  avatar: {
    type: String,
    required: true,
    trim: true,
    default: 'https://via.placeholder.com/200'
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    trim: true
  },
  friends: [{
    type: mongoose.Types.ObjectId,
    ref: 'User'
  }],
  created: {
    type: String,
    required: true
  }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;