const mongoose = require('mongoose');

const CommentSchema = mongoose.Schema({
  comment: {
    type: String,
    required: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  chapter: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Chapter'
  },
  pro: {
    type: Number
  },
  against: {
    type: Number
  },
  created: {
    type: String,
    required: true
  }
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;