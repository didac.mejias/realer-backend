const mongoose = require('mongoose');

const ChapterSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  content: {
    type: String,
    required: true,
    trim: true
  },
  stars: {
    type: Number,
    required: true,
    default: 0
  },
  bookbox: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'BookBox'
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  created: {
    type: String,
    required: true
  }
});

const Chapter = mongoose.model('Chapter', ChapterSchema);

module.exports = Chapter;