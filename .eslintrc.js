module.exports = {
  'env': {
    'browser': true,
    'es6': true,
    'node': true,
    'jest': true
  },
  'extends': [
    'eslint:recommended'
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly'
  },
  plugins: [
    'graphql'
  ],
  'parserOptions': {
    'ecmaVersion': 2018,
    'ecmaFeatures': {
      'arrowFunctions': true,
      'binaryLiterals': true,
      'blockBindings': true,
      'classes': true,
      'jsx': true
    }
  },
  'rules': {
    'indent': ['error', 2],
    'quotes': ['error', 'single'],
    'semi': ['error', 'always'],
    'space-before-function-paren': ['error', 'always'],
    'object-curly-spacing': ['error', 'always'],
    'key-spacing': ['error', { 'beforeColon': false }],
    'keyword-spacing': ['error', { 'before': true }],
    'padding-line-between-statements': [
      'error',
      { blankLine: 'always', prev: '*', next: 'return' },
      { blankLine: 'always', prev: ['const', 'let', 'var'], next: '*' },
      { blankLine: 'any', prev: ['const', 'let', 'var'], next: ['const', 'let', 'var'] }
    ],
    'comma-dangle': ['error', 'never'],
    'no-trailing-spaces': 'error'
  }
};