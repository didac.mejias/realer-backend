const Chapter = require('@models/Chapter');

const chapterQuerys = {
  getChapters: async (_, { id }) => {
    try {
      const chapters = await Chapter.find({ bookbox: id });

      return chapters;
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  },
  getChapter: async (_, { id }) => {
    try {
      const chapter = await Chapter.findById(id);

      if (!chapter) throw new Error('The chapter doesn\'t exists');

      return chapter;
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  }
};

exports.chapterQuerys = chapterQuerys;