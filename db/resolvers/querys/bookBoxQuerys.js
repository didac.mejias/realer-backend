const BookBox = require('@models/BookBox');

const bookBoxQuerys = {
  getAllBookBoxes: async () => {
    try {
      const bookBoxes = await BookBox.find();

      return bookBoxes;
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  },
  getBookBoxes: async (_, { id }) => {
    try {
      const bookBoxes = await BookBox.find({ author: id });

      return bookBoxes;
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  },
  getBookBox: async (_, { id }) => {
    try {
      const bookBox = await BookBox.findById(id);

      if (!bookBox) throw new Error('The bookBox doesn\'t exists');

      return bookBox;
    } catch (error) {
      throw new Error('Has been an error', error);
    }

  }
};

exports.bookBoxQuerys = bookBoxQuerys;