const User = require('../../../models/User');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');

require('dotenv').config({ path: 'variables.env' });

const createToken = (user, secretWord, expiresIn) => {
  const { id, nickname, email } = user;

  return jwt.sign({ id, nickname, email }, secretWord, { expiresIn });
};

const userMutations = {
  createUser: async (_, { input }) => {
    const { email, password } = input;
    const userExists = await User.findOne({ email });

    if (userExists) throw new Error('The user already exists');

    const salt = await bcryptjs.genSalt(10);

    input.password = await bcryptjs.hash(password, salt);

    try {
      let user = new User(input);

      user.created = moment().format('LL');

      const result = user.save();

      return result;

    } catch (error) {
      throw new Error('Has been an error', error);
    }
  },
  authenticate: async (_, { input }) => {
    const { email, password } = input;
    const userExists = await User.findOne({ email });

    if (!userExists) throw new Error('The user doesn\'t exists');

    const correctPassword = await bcryptjs.compare(password, userExists.password);

    if (!correctPassword) throw new Error('The password is not correct');

    return {
      token: createToken(userExists, process.env.SECRET_WORD, '24h')
    };
  }
};

exports.userMutations = userMutations;