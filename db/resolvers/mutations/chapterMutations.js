const Chapter = require('@models/Chapter');
const moment = require('moment');

const chapterMutations = {
  newChapter: async (_, { input }, ctx) => {
    let chapter = new Chapter(input);

    chapter.bookbox = '5f6895ec098a2c1cacd9da49';
    chapter.author = ctx.user.id;
    chapter.created = moment().format('LL');

    try {
      const result = await chapter.save();

      return result;
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  },
  updateChapter: async (_, { id, input }, ctx) => {
    try {
      let chapter = await Chapter.findById(id);

      if (!chapter) throw new Error('The chapter doesn\'t exists');

      if (chapter.author.toString() !== ctx.user.id) {
        throw new Error('You have to be logged in');
      }
      chapter = await Chapter.findOneAndUpdate({
        _id: id
      }, input, {
        new: true
      });
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  },
  deleteChapter: async (_, { id }, ctx) => {
    try {
      let chapter = await Chapter.findById(id);

      if (!chapter) throw new Error('The chapter doesn\'t exists');

      if (chapter.author.toString() !== ctx.user.id) {
        throw new Error('You have to be logged in');
      }
      await Chapter.findOneAndDelete({ _id: id });

      return 'Chapter deleted';
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  }
};

exports.chapterMutations = chapterMutations;