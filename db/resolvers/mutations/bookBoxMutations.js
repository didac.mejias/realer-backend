const BookBox = require('@models/BookBox');
const moment = require('moment');

const bookBoxMutations = {
  newBookBox: async (_, { input }, ctx) => {
    try {
      let bookBox = new BookBox(input);

      bookBox.author = ctx.user.id;
      bookBox.created = moment().format('LL');

      const result = bookBox.save();

      return result;
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  },
  updateBookBox: async (_, { id, input }) => {
    let bookBox = await BookBox.findById(id);

    if (!bookBox) throw new Error('The bookBox doesn\'t exists');

    bookBox = await BookBox.findOneAndUpdate({
      _id: id
    }, input, {
      new: true
    });

    return bookBox;
  },
  deleteBookBox: async (_, { id }) => {
    let bookBox = await BookBox.findById(id);

    if (!bookBox) throw new Error('The bookBox doesn\'t exists');

    await BookBox.findOneAndDelete({ _id: id });

    return 'Bookbox deleted';
  }
};

exports.bookBoxMutations = bookBoxMutations;