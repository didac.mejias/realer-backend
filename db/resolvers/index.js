const { userQuerys } = require('./querys/userQuerys');
const { bookBoxQuerys } = require('./querys/bookBoxQuerys');
const { chapterQuerys } = require('./querys/chapterQuerys');
const { userMutations } = require('./mutations/userMutations');
const { bookBoxMutations } = require('./mutations/bookBoxMutations');
const { chapterMutations } = require('./mutations/chapterMutations');


const resolvers = {
  Query: {
    ...userQuerys,
    ...bookBoxQuerys,
    ...chapterQuerys
  },
  Mutation: {
    ...userMutations,
    ...bookBoxMutations,
    ...chapterMutations
  }
};

module.exports = resolvers;