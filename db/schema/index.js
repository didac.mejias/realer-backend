const { gql } = require('apollo-server');
const { userDef } = require('./defs/user');
const { bookboxDef } = require('./defs/bookbox');
const { chapterDef } = require('./defs/chapter');
const { commentDef } = require('./defs/comments');


const root = gql`
    type Query{
        _empty: String
    }
    type Mutation {
        _empty: String
    }
`;

exports.typeDefs = [root, userDef, bookboxDef, chapterDef, commentDef];