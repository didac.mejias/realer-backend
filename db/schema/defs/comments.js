const { gql } = require('apollo-server');

exports.commentDef = gql`
    type Comment {
        id: ID
        comment: String
        author: ID
        chapter: ID
        pro: Int
        against: Int
        created: String
    }

    input CommentInput {
        comment: String!
        chapter: ID!
    }
`;