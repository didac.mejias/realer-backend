const { gql } = require('apollo-server');

exports.chapterDef = gql`
    type Chapter {
        id: ID
        title: String
        content: String
        bookbox: ID
        author: ID
        stars: Float
        created: String
    }

    input ChapterInput {
        title: String!
        content: String!
        bookbox: ID!
    }

    extend type Query {
        getChapters(id: ID!): [Chapter]
        getChapter(id: ID!): Chapter
    }

    extend type Mutation {
        newChapter(input: ChapterInput): Chapter
        updateChapter(id: ID!, input: ChapterInput): Chapter
        deleteChapter(id: ID!): String
    }
`;