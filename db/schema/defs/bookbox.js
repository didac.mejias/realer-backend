const { gql } = require('apollo-server');

exports.bookboxDef = gql`
    type BookBox {
        id: ID
        title: String
        description: String
        portrait: String
        language: String
        genre: [String]
        author: [String]
        created: String
    }

    input BookBoxInput {
        title: String!
        description: String!
        portrait: String
        language: String!
        genre: [String]!
    }

    extend type Query {
        getAllBookBoxes: [BookBox]
        getBookBoxes(id: ID!): [BookBox]
        getBookBox(id: ID!): BookBox
    }

    extend type Mutation {
        newBookBox(input: BookBoxInput): BookBox
        updateBookBox(id: ID!, input: BookBoxInput): BookBox
        deleteBookBox(id: ID!): String
    }
`;