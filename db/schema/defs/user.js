const { gql } = require('apollo-server');

exports.userDef = gql`
    type User {
      id: ID
      nickname: String
      avatar: String
      email: String
      friends: [User]
      created: String
    }

    type Token {
      token: String
    }

    input UserInput {
      nickname: String!
      email: String!
      password: String!
    }

    input AuthenticateUser {
      email: String!
      password: String!
    }

    extend type Query {
      getUser: User
    }

    extend type Mutation {
      createUser(input: UserInput): User
      authenticate(input: AuthenticateUser): Token
    }
`;