const jwt = require('jsonwebtoken');

require('dotenv').config({ path: 'variables.env' });

const getUser = async (req) => {
  const token = req.headers['authorization'] || '';

  if (token) {
    try {
      const user = await jwt.verify(token.replace('Bearer ', ''), process.env.SECRET_WORD);

      return user;
    } catch (error) {
      throw new Error('Has been an error', error);
    }
  }
};

module.exports = {
  getUser
};