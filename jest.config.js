module.exports = {
  verbose: true,
  preset: '@shelf/jest-mongodb',
  testEnvironment: 'node',
  collectCoverageFrom: [
    'config/**/*.{js,jsx}',
    'db/**/*.{js,jsx}',
    'helpers/**/*.{js,jsx}',
    'models/**/*.{js,jsx}',
    '!**/node_modules/**'
  ]
};