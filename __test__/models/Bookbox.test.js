const mongoose = require('mongoose');
const dbHandler = require('../dbHandler');
const BookBox = require('../../models/Bookbox');
const bookboxData = require('../mocks/Bookbox');

const newbookboxData = {
  title: 'Mocked BookBox title',
  description: 'Mocked description of the BookbBox',
  language: 'es-ES',
  genre: 'test',
  author: mongoose.Types.ObjectId(1234),
  created: '20 de Diciembre'
};

describe('BookBox Model should', () => {
  beforeAll(async () => {
    await dbHandler.connect();
  });

  afterEach(async () => {
    await dbHandler.clearDatabase();
  });

  afterAll(async () => {
    await dbHandler.closeDatabase();
  });

  it('create and save BookBox successfully', async () => {
    const validBookBox = new BookBox(newbookboxData);
    const saveBookBox = await validBookBox.save();

    expect(saveBookBox._id).toBeDefined();
    expect(saveBookBox.title).toBe(bookboxData.title);
    expect(saveBookBox.description).toBe(bookboxData.description);
    expect(saveBookBox.portrait).toBe(saveBookBox.portrait);
    expect(saveBookBox.language).toBe(saveBookBox.language);
    expect(saveBookBox.genre).toBe(saveBookBox.genre);
    expect(saveBookBox.created).toBe(saveBookBox.created);
  });

  it('return undefined in the field does not defined in schema', async () => {
    const BookBoxWithInvalidField = new BookBox({ ...newbookboxData, plot: 'some plot' });
    const savedBookBoxWithInvalidField = await BookBoxWithInvalidField.save();

    expect(savedBookBoxWithInvalidField._id).toBeDefined();
    expect(savedBookBoxWithInvalidField.plot).toBeUndefined();
  });

  it('return error if does not recive a required field', async () => {
    const BookBoxWithoutRequiredField = new BookBox({ title: 'only a title' });
    let err;

    try {
      const savedBookBoxWithoutRequiredField = await BookBoxWithoutRequiredField.save();

      err = savedBookBoxWithoutRequiredField;
    } catch (error) {
      err = error;
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.description).toBeDefined();
  });

});