const mongoose = require('mongoose');
const dbHandler = require('../dbHandler');
const User = require('../../models/User');
const UserData = require('../mocks/User');

const newUserData = {
  nickname: 'Mock user',
  avatar: 'https://via.placeholder.com/200',
  email: 'mockeduser@mock.com',
  password: 'pasword',
  created: '12 de Marzo'
};

describe('User Model should', () => {
  beforeAll(async () => {
    await dbHandler.connect();
  });

  afterEach(async () => {
    await dbHandler.clearDatabase();
  });

  afterAll(async () => {
    await dbHandler.closeDatabase();
  });

  it('create and save User successfully', async () => {
    const validUser = new User(newUserData);
    const savedUser = await validUser.save();

    expect(savedUser._id).toBeDefined();
    expect(savedUser.nickname).toBe(UserData.nickname);
    expect(savedUser.avatar).toBe(UserData.avatar);
    expect(savedUser.email).toBe(UserData.email);
    expect(savedUser.password).toBe(UserData.password);
    expect(savedUser.created).toBe(UserData.created);
  });

  it('return undefined in the field does not defined in schema', async () => {
    const UserWithInvalidField = new User({ ...newUserData, plot: 'some plot' });
    const savedUserWithInvalidField = await UserWithInvalidField.save();

    expect(savedUserWithInvalidField._id).toBeDefined();
    expect(savedUserWithInvalidField.plot).toBeUndefined();
  });

  it('return error if does not recive a required field', async () => {
    const UserWithoutRequiredField = new User({});
    let err;

    try {
      const savedUserWithoutRequiredField = await UserWithoutRequiredField.save();

      err = savedUserWithoutRequiredField;
    } catch (error) {
      err = error;
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.nickname.kind).toBe('required');
  });

});