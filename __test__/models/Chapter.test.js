const mongoose = require('mongoose');
const dbHandler = require('../dbHandler');
const Chapter = require('../../models/Chapter');
const chapterData = require('../mocks/Chapter');

const newChapterData = {
  title: 'Chapter mock title',
  content: 'Chapter mock content',
  stars: 5,
  bookbox: mongoose.Types.ObjectId(1234),
  author: mongoose.Types.ObjectId(1234),
  created: '3 de Enero'
};

describe('Chapter Model should', () => {
  beforeAll(async () => {
    await dbHandler.connect();
  });

  afterEach(async () => {
    await dbHandler.clearDatabase();
  });

  afterAll(async () => {
    await dbHandler.closeDatabase();
  });

  it('create and save Chapter successfully', async () => {
    const validChapter = new Chapter(newChapterData);
    const savedChapter = await validChapter.save();

    expect(savedChapter._id).toBeDefined();
    expect(savedChapter.title).toBe(chapterData.title);
    expect(savedChapter.content).toBe(chapterData.content);
    expect(savedChapter.stars).toBe(chapterData.stars);
    expect(savedChapter.created).toBe(chapterData.created);
  });

  it('return undefined in the field does not defined in schema', async () => {
    const ChapterWithInvalidField = new Chapter({ ...newChapterData, plot: 'some plot' });
    const savedChapterWithInvalidField = await ChapterWithInvalidField.save();

    expect(savedChapterWithInvalidField._id).toBeDefined();
    expect(savedChapterWithInvalidField.plot).toBeUndefined();
  });

  it('return error if does not recive a required field', async () => {
    const ChapterWithoutRequiredField = new Chapter({ title: 'only a title' });
    let err;

    try {
      const savedChapterWithoutRequiredField = await ChapterWithoutRequiredField.save();

      err = savedChapterWithoutRequiredField;
    } catch (error) {
      err = error;
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.content.kind).toBe('required');
  });

});