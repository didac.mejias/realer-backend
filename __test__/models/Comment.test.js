const mongoose = require('mongoose');
const dbHandler = require('../dbHandler');
const Comment = require('../../models/Comment');
const CommentData = require('../mocks/Comment');

const newCommentData = {
  comment: 'Mocked comment',
  author: mongoose.Types.ObjectId(1234),
  chapter: mongoose.Types.ObjectId(1234),
  pro: 34,
  against: 25,
  created: '5 de Enero'
};

describe('Comment Model should', () => {
  beforeAll(async () => {
    await dbHandler.connect();
  });

  afterEach(async () => {
    await dbHandler.clearDatabase();
  });

  afterAll(async () => {
    await dbHandler.closeDatabase();
  });

  it('create and save Comment successfully', async () => {
    const validComment = new Comment(newCommentData);
    const savedComment = await validComment.save();

    expect(savedComment._id).toBeDefined();
    expect(savedComment.comment).toBe(CommentData.comment);
    expect(savedComment.pro).toBe(CommentData.pro);
    expect(savedComment.against).toBe(CommentData.against);
    expect(savedComment.created).toBe(CommentData.created);
  });

  it('return undefined in the field does not defined in schema', async () => {
    const CommentWithInvalidField = new Comment({ ...newCommentData, plot: 'some plot' });
    const savedCommentWithInvalidField = await CommentWithInvalidField.save();

    expect(savedCommentWithInvalidField._id).toBeDefined();
    expect(savedCommentWithInvalidField.plot).toBeUndefined();
  });

  it('return error if does not recive a required field', async () => {
    const CommentWithoutRequiredField = new Comment({});
    let err;

    try {
      const savedCommentWithoutRequiredField = await CommentWithoutRequiredField.save();

      err = savedCommentWithoutRequiredField;
    } catch (error) {
      err = error;
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.comment.kind).toBe('required');
  });

});