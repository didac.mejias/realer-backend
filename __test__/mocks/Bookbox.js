module.exports = {
  _id: 123,
  title: 'Mocked BookBox title',
  description: 'Mocked description of the BookbBox',
  portrait: 'https://picsum.photos/200',
  language: 'es-ES',
  genre: 'test',
  author: 1234,
  created: '20 de Diciembre'
};